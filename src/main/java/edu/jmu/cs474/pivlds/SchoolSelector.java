package edu.jmu.cs474.pivlds;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Pattern;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class SchoolSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String like_text;

	School current = new School();

	public School getCurrent() {
		return current;
	}

	public String getLikeText() {
		return like_text;
	}

	public void setLikeText(String pattern) {
		like_text = pattern;
	}

	public void doLike() {
		selectLike(like_text);
	}

	public void selectLike(String pattern) {
		try {
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement("Select * from school_like(?) NATURAL JOIN division");) {
				ps.setString(1, pattern);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next())
						current.read(rs);
					else
						current.reset();
				}
			}
		} catch (Exception e) {
			current.reset(e);
		}
	}

	public void doKey() {
		selectKey(like_text);
	}

	public void selectKey(String pattern) {
		try {
			pattern = pattern.trim();
			if (!Pattern.matches("\\d+", pattern)) 
				throw new Exception("[NOT NUMERIC] " + pattern);
			
			int key = Integer.valueOf(pattern);
			try (Connection c = PivldsDb.getConnection();
					PreparedStatement ps = c.prepareStatement("Select * from school(?) NATURAL JOIN division");) {
				ps.setInt(1, key);
				try (ResultSet rs = ps.executeQuery();) {
					rs.next();
					current.read(rs);
				}
			}
		} catch (Exception e) {
			current.reset(e);
		}
	}

}
