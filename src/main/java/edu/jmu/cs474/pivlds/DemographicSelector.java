package edu.jmu.cs474.pivlds;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class DemographicSelector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Demographic current = new Demographic();

	public Demographic getCurrent() {
		return current;
	}

	public Demographic.Race[] getRaces() {
		return Demographic.Race.values();
	}

	public Demographic.Gender[] getGenders() {
		return Demographic.Gender.values();
	}

	public Demographic.Disabil[] getDisabils() {
		return Demographic.Disabil.values();
	}

	public Demographic.Disadva[] getDisadvas() {
		return Demographic.Disadva.values();
	}

	public Demographic.LEP[] getLeps() {
		return Demographic.LEP.values();
	}

}
