package edu.jmu.cs474.pivlds;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

public class School implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Integer sch_pk;
	Integer sch_num;
	Integer div_num;
	String div_name;
	String sch_name;

	public Integer getSch_pk() {
		return sch_pk;
	}

	public Integer getSch_num() {
		return sch_num;
	}

	public Integer getDiv_num() {
		return div_num;
	}

	public String getDiv_name() {
		return div_name;
	}

	public String getSch_name() {
		return sch_name;
	}

	public void read(ResultSet rs) throws SQLException {
		sch_pk = rs.getInt("sch_pk");
		sch_num = rs.getInt("sch_num");
		div_num = rs.getInt("div_num");
		sch_name = rs.getString("sch_name");
		div_name = rs.getString("div_name");
	}

	public void reset() {
		sch_pk = null;
		sch_num = null;
		div_num = null;
		sch_name = null;
		div_name = null;
	}

	public void reset(Exception e) {
		reset();
		e.printStackTrace();
	}

}
