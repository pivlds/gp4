package edu.jmu.cs474.pivlds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.postgresql.Driver;

public class PivldsDb {

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Driver getPostgresDriver() {
		Enumeration<java.sql.Driver> drivers = DriverManager.getDrivers();

		while (drivers.hasMoreElements()) {
			java.sql.Driver d = drivers.nextElement();
			if (d instanceof Driver) {
				return (Driver) d;
			}
		}
		return null;
	}

	@SuppressWarnings("serial")
	public static LinkedList<String> dbUrls = new LinkedList<String>() {
		{
			add("jdbc:postgresql://db.cs.jmu.edu:5432/pivlds");
			add("jdbc:postgresql://localhost:5432/pivlds");
			add("jdbc:postgresql://localhost:6432/pivlds");
		}
	};

	private static String postgresUrl = findTargetURL();

	public static String findTargetURL() {
		System.out.println("TESTING URLS:");
		for (String url : dbUrls) {
			SQLException e = testPostgresHost(url);
			System.out.println((e == null ? "[Bingo] " : "[" + connectionFailureString(e) + "]") + "\t" + url);
			if (e == null)
				return url;
		}
		System.out.println("No Matches");
		return null;
	}

	public static SQLException testPostgresHost(String host) {
		return testPostgresHost(host, loadUserProperties());
	}

	public static SQLException testPostgresHost(String host, Properties props_user) {
		Driver d = getPostgresDriver();

		Properties props_conn = new Properties();

		props_conn.put("user", props_user.get("dbuser"));
		props_conn.put("password", props_user.get("dbpass"));
		props_conn.put("loginTimeout", "" + 1);

		try (Connection c = d.connect(host, props_conn)) {
		} catch (SQLException e) {
			return e;
		}
		return null;
	}

	public static String connectionFailureString(SQLException e) {
		if (e.getMessage().contains("timed out"))
			return "Timeout";
		if (e.getMessage().contains("refused"))
			return "Refused";
		return "" + e.getMessage();
	}

	public static Properties loadUserProperties() {
		Properties props = new Properties();

		try {
			props.load(new FileInputStream(getCredentialsFile()));
		} catch (FileNotFoundException e) {
			props = promptCredentials();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return props;
	}

	public static File getCredentialsFile() {
		return new File(System.getProperty("user.home"), "pivldsauth");
	}

	public static Connection getConnection() throws SQLException {
		Properties props = loadUserProperties();
		try {
			return DriverManager.getConnection(postgresUrl, props.getProperty("dbuser"), props.getProperty("dbpass"));
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage());
			return null;
		}
	}

	public static Properties promptCredentials() {
		String user = JOptionPane.showInputDialog("Enter user name for database");
		String password = JOptionPane.showInputDialog("Enter password for database");

		Properties p = new Properties();
		p.setProperty("dbuser", user);
		p.setProperty("dbpass", password);

		try {
			p.store(new FileOutputStream(getCredentialsFile()), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}

	/**
	 * Tests the connection to console.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		try (Connection c = getConnection();
				PreparedStatement ps = c.prepareStatement(
						"SELECT tablename FROM pg_catalog.pg_tables WHERE tableowner not like 'postgres'");
				ResultSet rs = ps.executeQuery();) {
			System.out.println("Listing Tables");
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
		}
	}
}
