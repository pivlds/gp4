package edu.jmu.cs474.pivlds;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class RunTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	boolean show = false;
	
	public void toggleShow() {
		show = !show;
	}
	
	public boolean getShow() {
		return show;
	}
	
}
