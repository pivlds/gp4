
function test() {
	alert("Hello")
}

function readTable(selector) {
	var chart = document.querySelector(selector);

	console.log(chart);

	var options = chart.dataset;

	console.log(options);

	// now process table

	var table = document.getElementById(options.table);
	if (table == null)
		return null;
	
	var tbody = table.tBodies[0];
	var thead = table.tHead;
	var trows = tbody.rows;

	console.log(table);
	console.log(thead);
	console.log(tbody);

	var hrow = options.headerRow;

	var tcols = thead.rows[hrow].cells;
	var cols = [];

	// note the < entity because of jsf pre-processor
	for (var i = 0; i < tcols.length; i++) {
		cols[cols.length] = tcols[i].innerText.trim().replace(/\s+/, " ");
	}
	console.log(tcols);
	console.log(cols);

	var rows = [];
	for (var r = 0; r < trows.length; r++) {
		rows[rows.length] = rowToArray(trows[r]);
	}

	var r = {
		columns : cols,
		rows : rows,
		options : options
	};

	filterColumns(r, readColumns(options.columns));
	convertColumns(r, options.axisXType)
	console.log(r);
	return r;
}

function convertColumns(table, format) {
	var parser = format == 'int' ? parseInt : parseFloat;

	for (var r = 0; r < table.rows.length; r++) {
		for (var c = 1; c < table.rows[r].length; c++)
			table.rows[r][c] = parser(table.rows[r][c]);
	}
}

function filterColumns(table, useCols) {
	for (var c = 1; c < table.columns.length; c++) {
		var tcol = table.columns[c];
		var found = false;
		for (var uc = 0; uc < useCols.length; uc++) {

			var ucol = useCols[uc];

			if (ucol == tcol) {
				found = true;
				break;
			}
		}

		if (!found) {
			table.columns.splice(c, 1)
			for (var r = 0; r < table.rows.length; r++) {
				table.rows[r].splice(c, 1);
			}
			c--;
		}
	}
}

function rowToArray(row) {
	var r = [];
	for (var i = 0; i < row.cells.length; i++) {
		r[r.length] = row.cells[i].innerText;
	}
	return r;
}

function readColumns(columnList) {
	var r = columnList.split(/[,]/);
	for (var i = 0; i < r.length; i++) {
		r[i] = r[i].trim();
	}
	return r;
}

function drawChart(chartSelector) {
	var target_div = document.querySelector(chartSelector);
	if (target_div == null)
		return;
	target_div.className = 'chart'

	var tdata = readTable(chartSelector);
	if (tdata == null)
		return;

	var arr = [ tdata.columns ].concat(tdata.rows);

	var data = google.visualization.arrayToDataTable(arr);

	// Set chart options
	var options = {}
	options.title = tdata.options.title;
	options.hAxis = {
		title : tdata.options.axisX
	}
	options.vAxis = {
		title : tdata.options.axisY
	}
	options.legend = {
		position : "top"
	}

	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.LineChart(target_div);
	chart.draw(data, options);
}